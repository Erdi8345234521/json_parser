﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JSON_Parser
{
    public partial class DtAttachmentInfo
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Image")]
        public string Image { get; set; }

        [JsonProperty("TypeAttach")]
        public string TypeAttach { get; set; }

        [JsonProperty("ForWeapon")]
        public string ForWeapon { get; set; }

        [JsonProperty("UnicKey")]
        public string UnicKey { get; set; }

        [JsonProperty("ActorAttach")]
        public string ActorAttach { get; set; }

        [JsonProperty("Accessibility method")]
        public string AccessibilityMethod { get; set; }
    }
}
