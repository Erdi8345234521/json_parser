﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;

namespace JSON_Parser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string DtWeaponInfo_v = string.Empty;
        string DTSkillsInfo_v = string.Empty;
        string DTGearInfo_v = string.Empty;
        string DtAttachmentInfo_v = string.Empty;

        struct Data
        {
            public string Name;
            public string ID;
            public int AccessibilityMethod;
        }

        //Data[] data = new Data[100];

        List<Data> data = new List<Data>(10);
        
        private void button1_Click(object sender, EventArgs e)
        {

            openFileDialog1.Filter = "JSON files(*.json)|*.json|All files(*.*)|*.*";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string textFromFile = string.Empty;
                //MessageBox.Show(openFileDialog1.SafeFileName);              
                using (FileStream fstream = File.OpenRead(openFileDialog1.FileName))
                {
                    // преобразуем строку в байты
                    byte[] array = new byte[fstream.Length];
                    // считываем данные
                    fstream.Read(array, 0, array.Length);
                    // декодируем байты в строку
                    textFromFile = System.Text.Encoding.Default.GetString(array);
                }
                switch (openFileDialog1.SafeFileName)
                {
                    case "DT_WeaponInfo.json":
                        DtWeaponInfo_v = textFromFile;
                        break;

                    case "DT_SkillsInfo.json":
                        DTSkillsInfo_v = textFromFile;
                        break;

                    case "DT_GearInfo.json":
                        DTGearInfo_v = textFromFile;
                        break;

                    case "DT_AttachmentInfo.json":
                        DtAttachmentInfo_v = textFromFile;
                        break;
                }
                richTextBox1.Text += string.Format("Файл {0} загружен\n", openFileDialog1.SafeFileName);
            }
        }

        public int switch_AccessibilityMethod(string AccessibilityMethod)
        {
            int x = 0;
            switch (AccessibilityMethod)
            {
                case "initial":
                    x = 0;
                    break;

                case "Donat":
                    x = 1;
                    break;

                case "ForBetaTesters":
                    x = 2;
                    break;
            }
            return x;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            
            //datas[0].ID = "lol"; 
            //MessageBox.Show(datas[0].ID.ToString());
            //MessageBox.Show(DtWeaponInfo_v);
            int x = 0;
            if (DtWeaponInfo_v != null)
            {
                var dtWeaponInfo = JsonConvert.DeserializeObject<List<DtWeaponInfo>>(DtWeaponInfo_v);
                foreach (var item in dtWeaponInfo)
                {
                    Data data_1;
                    data_1.ID = item.UnicKey;
                    data_1.Name = item.Name;
                    data_1.AccessibilityMethod = switch_AccessibilityMethod(item.AccessibilityMethod);
                    data.Add(data_1);                   
                    richTextBox2.Text += string.Format("Name:{0}; UnicKey:{1}\n ;E_AccessibilityMethod: {2}", item.Name, item.UnicKey, switch_AccessibilityMethod(item.AccessibilityMethod));
                    x++;
                }
            }    
            
            if (DTGearInfo_v != null)
            {
                var DTGearInfo = JsonConvert.DeserializeObject<List<DtGearInfo>>(DTGearInfo_v);
                foreach (var item in DTGearInfo)
                {
                    Data data_1;
                    data_1.ID = item.UnicKey.ToString(  );
                    data_1.Name = item.Name;
                    data_1.AccessibilityMethod = switch_AccessibilityMethod(item.AccessibilityMethod);
                    data.Add(data_1);
                    richTextBox2.Text += string.Format("Name:{0}; UnicKey:{1}\n ;E_AccessibilityMethod: {2}", item.Name, item.UnicKey, switch_AccessibilityMethod(item.AccessibilityMethod));
                    x++;
                }
            }

            if (DtAttachmentInfo_v != null)
            {
                var DtAttachmentInfo = JsonConvert.DeserializeObject<List<DtAttachmentInfo>>(DtAttachmentInfo_v);
                foreach (var item in DtAttachmentInfo)
                {
                    Data data_1;
                    data_1.ID = item.UnicKey;
                    data_1.Name = item.Name;
                    data_1.AccessibilityMethod = switch_AccessibilityMethod(item.AccessibilityMethod);
                    data.Add(data_1); 
                    richTextBox2.Text += string.Format("Name:{0}; UnicKey:{1}\n ;E_AccessibilityMethod: {2}", item.Name, item.UnicKey, switch_AccessibilityMethod(item.AccessibilityMethod));
                    x++;
                }
            }

            if (DTSkillsInfo_v != null)
            {
                var DTSkillsInfo = JsonConvert.DeserializeObject<List<DtSkillsInfo>>(DTSkillsInfo_v);
                foreach (var item in DTSkillsInfo)
                {
                    Data data_1;
                    data_1.ID = item.UnicKey.ToString();
                    data_1.Name = item.Name;
                    data_1.AccessibilityMethod = switch_AccessibilityMethod(item.AccessibilityMethod);
                    data.Add(data_1); 
                    richTextBox2.Text += string.Format("Name:{0}; UnicKey:{1}\n ;E_AccessibilityMethod: {2}", item.Name, item.UnicKey, switch_AccessibilityMethod(item.AccessibilityMethod));
                    x++;
                }
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string serverName = "26.133.136.203"; // Адрес сервера (для локальной базы пишите "localhost")
            string userName = "root"; // Имя пользователя
            string dbName = "warponies"; //Имя базы данных
            string port = "3307"; // Порт для подключения
            string password = ""; // Пароль для подключения
            string connStr = "server=" + serverName +
            ";user=" + userName +
            ";database=" + dbName +
            ";port=" + port +
            ";password=" + password + ";";

            

            using (MySqlConnection connectionAccountDataVerification = new MySqlConnection(connStr))
            {
                connectionAccountDataVerification.Open();
                MessageBox.Show(data.Count.ToString());
                for (int x = 0; x < data.Count; x++)
                {
                    string sql3 = "INSERT INTO `gameitemsinfo` (`ItemId`, `AccessibilityMethod`) VALUES ('" + data[x].ID + "','" + data[x].AccessibilityMethod +  "')";
                    MySqlCommand sqlcom3 = new MySqlCommand(sql3, connectionAccountDataVerification);
                    sqlcom3.ExecuteNonQuery();
                    //MessageBox.Show(data[x].ID);
                }
                MessageBox.Show("Операция выполнена");
            }

        }
    }
}
