﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace JSON_Parser
{
    public partial class DtGearInfo
    {
        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        [JsonProperty("Image")]
        public string Image { get; set; }

        [JsonProperty("TypeGear")]
        public string TypeGear { get; set; }

        [JsonProperty("ForClass")]
        public string ForClass { get; set; }

        [JsonProperty("UnicKey")]
        public long UnicKey { get; set; }

        [JsonProperty("ActorWeapon")]
        public string ActorWeapon { get; set; }

        [JsonProperty("Accessibility method")]
        public string AccessibilityMethod { get; set; }
    }
}
